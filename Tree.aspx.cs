﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace dbWebFormClient
{
    public partial class Tree : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                return;
            }

            DataTable dbsTable = new DataTable("dbsTable");
            DataRow dbRow;

            dbsTable.Columns.Add(new DataColumn("ID", typeof(int)));
            dbsTable.Columns.Add(new DataColumn("Name", typeof(String)));

            DataSet dbsDS = new DataSet("dbsDS");
            dbsDS.Tables.Add(dbsTable);

            for (int i=0; i < ConfigurationManager.ConnectionStrings.Count; i++)
            {
                ConnectionStringSettings cs = ConfigurationManager.ConnectionStrings[i];

                dbRow = dbsTable.NewRow();

                dbRow.SetField<int>("ID", i);
                dbRow.SetField<String>("Name", cs.Name);

                dbsTable.Rows.Add(dbRow);
            }

            dbRepeater.DataSource = dbsDS;
            dbRepeater.DataMember = "dbsTable";
            dbRepeater.DataBind();
        }

        protected void dbRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Select")
            {
                var csId = int.Parse(e.CommandArgument.ToString());
                var tablesTable = new DataTable("tbTable");
                tablesTable.Columns.Add(new DataColumn("CsId", typeof(int)));
                tablesTable.Columns.Add(new DataColumn("Name", typeof(string)));
                DataRow tbRow;

                var tablesDSId = "tablesDS" + csId;
                DataSet tablesDS = new DataSet(tablesDSId);
                tablesDS.Tables.Add(tablesTable);

                var cs = ConfigurationManager.ConnectionStrings[csId];

                using (SqlConnection conn = new SqlConnection(cs.ToString()))
                {
                    try
                    {
                        conn.Open();
                        SqlCommand cmd = new SqlCommand("SELECT * FROM information_schema.tables WHERE TABLE_TYPE='BASE TABLE'", conn);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.HasRows)
                        {

                            while (reader.Read())
                            {
                                tbRow = tablesTable.NewRow();
                                tbRow.SetField<int>("CsId", csId);
                                tbRow.SetField<String>("Name", reader.GetString(2));
                                tablesTable.Rows.Add(tbRow);
                            }
                        }

                        conn.Close();
                    }
                    catch (Exception ex)
                    {
                        //((HtmlGenericControl)e.Item.FindControl("AlertBlock")).Visible = true;
                        //((HtmlGenericControl)e.Item.FindControl("AlertBlock")).InnerText = ex.ToString();
                    }
                }

                Repeater tablesRepeater = (Repeater)e.Item.FindControl("TablesRepeater");
                tablesRepeater.DataSource = tablesDS;
                tablesRepeater.DataMember = "tbTable";
                tablesRepeater.DataBind();
            }
        }

        protected void TablesRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "SelectTable")
            {
                var terms = e.CommandArgument.ToString().Split('\\');
                var csId = int.Parse(terms[0]);
                var tableName = terms[1];

                var cs = ConfigurationManager.ConnectionStrings[csId];

                using (SqlConnection conn = new SqlConnection(cs.ToString()))
                {
                    try
                    {
                        conn.Open();
                        String sql = String.Format("SELECT TOP 10 * FROM {0}", tableName);
                        var command = new SqlCommand(sql, conn);
                        var da = new SqlDataAdapter(command);
                        var dataTable = new DataTable();
                        da.Fill(dataTable);
                        Grid.DataSource = dataTable;
                        Grid.DataBind();
                        conn.Close();
                    }
                    catch (Exception)
                    {
                    }
                }
            } else if (e.CommandName == "DeleteTable")
            {
                var terms = e.CommandArgument.ToString().Split('\\');
                var csId = int.Parse(terms[0]);
                var tableName = terms[1];

                var cs = ConfigurationManager.ConnectionStrings[csId];

                using (SqlConnection conn = new SqlConnection(cs.ToString()))
                {
                    try
                    {
                        conn.Open();
                        String sql = String.Format("DROP TABLE {0}", tableName);
                        var command = new SqlCommand(sql, conn);
                        command.ExecuteNonQuery();
                        conn.Close();
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
        }

        /*protected void ConnectionStringItem_Click(object sender, EventArgs e)
        {
            var dbQueryParam = Request.QueryString["db"];


            DataTable tablesTable = new DataTable("tbTable");
            tablesTable.Columns.Add(new DataColumn("Name", typeof(string)));

            DataRow tbRow;

                using (SqlConnection conn = new SqlConnection(cs.ToString()))
                {
                    try
                    {
                        conn.Open();
                        SqlCommand cmd = new SqlCommand("SELECT * FROM information_schema.tables WHERE TABLE_TYPE='BASE TABLE'", conn);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.HasRows)
                        {

                            while (reader.Read())
                            {
                                tbRow = tablesTable.NewRow();
                                tbRow.SetField<String>("Name", reader.GetString(2));
                                tablesTable.Rows.Add(tbRow);
                            }
                        }

                        conn.Close();
                    }
                    catch (Exception)
                    {
                    }
                }

        //dbRow.SetField<DataTable>("Tables", tablesTable);
    }*/
    }
}