﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Tree.aspx.cs" Inherits="dbWebFormClient.Tree" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
    <form id="form1" runat="server">
    <div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-3">
            <br />
            <h5>Connection strings <small>click to select</small></h5>
            <ul class="nav nav-pills nav-stacked nav-pills-stacked-example">
            <asp:Repeater ID="dbRepeater" runat="server" OnItemCommand="dbRepeater_ItemCommand">
                <ItemTemplate>
                    <li id="NavItem" runat="server">
                    <p>
                        <a href="javascript:void(0);">
                            <asp:Button
                                Text=<%# DataBinder.Eval(Container.DataItem,"Name") %>
                                CommandArgument=<%# DataBinder.Eval(Container.DataItem,"ID") %>
                                CommandName="Select"
                                ID="SelectBtn"
                                runat="server"
                                Class="btn btn-default btn-block"
                                />
                        </a>
                    </p>     
                    <ul runat="server" id="TablesRepeaterContainer" class="list-group">
                        <asp:Repeater runat="server" ID="TablesRepeater" OnItemCommand="TablesRepeater_ItemCommand">
                            <ItemTemplate>
                            <li class="list-group-item">
                                <a href="javascript:void(0);">
                                    <asp:Button
                                        Text=<%# DataBinder.Eval(Container.DataItem,"Name") %>
                                        CommandArgument=<%# DataBinder.Eval(Container.DataItem, "CsId").ToString() + "\\" + DataBinder.Eval(Container.DataItem,"Name") %>
                                        CommandName="SelectTable"
                                        ID="SelectTableBtn"
                                        runat="server"
                                        Class="btn btn-link"
                                        />
                                    <asp:Button
                                        Text="delete"
                                        CommandArgument=<%# DataBinder.Eval(Container.DataItem, "CsId").ToString() + "\\" + DataBinder.Eval(Container.DataItem,"Name") %>
                                        CommandName="DeleteTable"
                                        ID="DeleteTableBtn"
                                        runat="server"
                                        Class="btn btn-default pull-right"
                                        />
                            </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                    </li>
                </ItemTemplate>
            </asp:Repeater>
            </ul>
        </div>
        <div class="col-sm-9 col-md-9">
            <h4>Table data <small>select table first</small></h4>
            <asp:GridView ID="Grid" runat="server" CssClass="table table-hover table-striped">
                <EmptyDataTemplate>Empty table</EmptyDataTemplate>
            </asp:GridView>
        </div>
    </div>
    </div>
    </form>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
